# Example from AZ
FROM microsoft/dotnet:2.1-aspnetcore-runtime
LABEL maintainer="AZ"
RUN apt-get update
RUN apt-get install -y unzip
RUN mkdir /source
RUN mkdir /app
RUN curl -o /source/source.zip https://www.zanibike.net/AspnetCoreLinux2.zip
RUN cd /source/ && unzip -q source.zip && rm -rf source.zip  && mv * /app
RUN chmod +x /app/start.sh
EXPOSE 5000
CMD ["/app/start.sh"]